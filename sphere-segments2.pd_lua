local sphere = pd.Class:new():register("sphere-segments2")

local function mmul(l, r)
  local v = { }
  for i = 0,3 do
    local f = 0.0;
    for j = 0,3 do
      f = f + l[i*4+j] * r[j+1];
    end
    v[i+1] = f;
  end
  return v
end

local function mmulm(l, r)
  local m = { }
  for i = 0,3 do
    for j = 0,3 do
      local f = 0.0;
      for k = 0,3 do
        f = f + l[i*4+k] * r[k*4+j];
      end
      m[i*4+j] = f
    end
  end
  return m
end

-- /* quaternion norm squared */
local function qnorm2(p)
  return p[0]*p[0]+p[1]*p[1] + p[2]*p[2] + p[3]*p[3];
end

-- /* quaternion norm */
local function qnorm(p)
  return math.sqrt(qnorm2(p));
end

-- /* quaternion inverse: r := p^-1 */
-- /* p^-1 = p* / |p|^2 */
local function qinv(p)
  local pn2 = qnorm2(p);
  if pn2 == 0 then pn2 = 1 end
  local r = { }
  r[0] =  p[0] / pn2;
  r[1] = -p[1] / pn2;
  r[2] = -p[2] / pn2;
  r[3] = -p[3] / pn2;
  return r;
end

local function qadd(p, q)
  local r = { }
  r[0] = p[0] + q[0]
  r[1] = p[1] + q[1]
  r[2] = p[2] + q[2]
  r[3] = p[3] + q[3]
  return r
end

-- /* quaternion multiplication: r := q * p */
local function qmul(q, p)
  local q1, q2, q3, q4, p1, p2, p3, p4, r1, r2, r3, r4;
  q1 = q[0]; q2 = q[1]; q3 = q[2]; q4 = q[3];
  p1 = p[0]; p2 = p[1]; p3 = p[2]; p4 = p[3];
  r1 = q1 * p1 - q2 * p2 - q3 * p3 - q4 * p4;
  r2 = q2 * p1 + q1 * p2 - q4 * p3 + q3 * p4;
  r3 = q3 * p1 + q4 * p2 + q1 * p3 - q2 * p4;
  r4 = q4 * p1 - q3 * p2 + q2 * p3 + q1 * p4;
  local r = { }
  r[0] = r1; r[1] = r2; r[2] = r3; r[3] = r4;
  return r
end

local function qmuls(p, s)
  local r = { }
  r[0] = p[0] * s
  r[1] = p[1] * s
  r[2] = p[2] * s
  r[3] = p[3] * s
  return r
end

local function qnormalize(p)
  local n = qnorm(p)
  if n ~= 0 then
    return qmuls(p, 1/n)
  else
    return qmuls(p, 0)
  end
end

-- /* quaternion exponentiation: r := exp(p) */
-- /* exp(s;v) = exp(s) (cos(|v|) ; v sin(|v|) / |v|) */
local function qexp(p)
  local s, v0, v1, v2, vn, se, vns, vnc;
  s   = p[0];
  v0  = p[1];
  v1  = p[2];
  v2  = p[3];
  vn  = math.sqrt(v0*v0 + v1*v1 + v2*v2);
  vnc = math.cos(vn);
  if vn ~= 0 then
    vns = math.sin(vn) / vn;
  else
    vns = 0
  end
  se  = math.exp(s);
  local r = { }
  r[0] = se * vnc;
  r[1] = v0 * vns;
  r[2] = v1 * vns;
  r[3] = v2 * vns;
  return r
end

-- /* quaternion logarithm: r := log(p) */
-- /* ln(q@(s;v)) = (ln(|q|) ; v acos(s / |q|) / |v|) */
local function qlog(p)
  local s, v0, v1, v2, vn2, vn, qn, qnl, sac;
  s   = p[0];
  v0  = p[1];
  v1  = p[2];
  v2  = p[3];
  vn2 = v0*v0 + v1*v1 + v2*v2;
  vn  = math.sqrt(vn2);
  qn  = math.sqrt(s*s + vn2);
  qnl = math.log(qn);
  sac = math.acos(s / qn) / vn;
  local r = { }
  r[0] = qnl;
  r[1] = v0 * sac;
  r[2] = v1 * sac;
  r[3] = v2 * sac;
  return r;
end

-- /* quaternion scalar power: r := p^f */
-- /* p^f = exp(ln(p) f) */
local function qpows(p, f)
  local l = qlog(p);
  l[0] = l[0] * f;
  l[1] = l[1] * f;
  l[2] = l[2] * f;
  l[3] = l[3] * f;
  return qexp(l);
end

-- /* quaternion slerp: r := Slerp(p,q,t) */
-- /* Slerp(p,q;t) = (q p^-1)^t p */
local function qslerp(p, q, t)
  return qmul(qpows(qmul(q, qinv(p)), t), p)
end

-- /* 4x4 rotation matrix from two axes and an angle */
local function mrot(i,j,t)
  local m = { }
  local k = 0
  for ii = 0,3 do for jj = 0,3 do
    if     (ii == i and jj == i) or (ii == j and jj == j) then
      m[k] =  math.cos(t)
    elseif (ii == i and jj == j) then
      m[k] = -math.sin(t)
    elseif (ii == j and jj == i) then
      m[k] =  math.sin(t)
    elseif (ii == jj) then
      m[k] =  1
    else
      m[k] =  0
    end
    k = k + 1
  end end
  return m
end

--[[
The cross product is defined by the formula[2]

    \mathbf{a} \times \mathbf{b} = a b \sin \theta \ \mathbf{\hat{n}}

where θ is the measure of the smaller angle between a and b (0° ≤ θ ≤ 180°),
a and b are the magnitudes of vectors a and b, and \scriptstyle\mathbf{\hat{n}}
is a unit vector perpendicular to the plane containing a and b in the direction
given by the right-hand rule as illustrated.
    \mathbf{i}a_2b_3 + \mathbf{j}a_3b_1 + \mathbf{k}a_1b_2
  - \mathbf{i}a_3b_2 - \mathbf{j}a_1b_3 - \mathbf{k}a_2b_1. 

--]]

local function cross(a, b)
  return { a[2]*b[3]-a[3]*b[2], a[3]*b[1]-a[1]*b[3], a[1]*b[2] - a[2]*b[1] }
end

--[[

http://www.gamedev.net/community/forums/topic.asp?topic_id=429507&whichpage=1&#2856228

@The OP: There's actually a very nice algorithm for finding the quaternion that
rotates one vector onto another that has a couple of advantages over the
'standard' method, namely that it works with vectors of arbitrary length,
and that it handles uniformly (and robustly) the case where the vectors are
aligned and nearly parallel.

Here's the algorithm in pseudocode:

quaternion q;
vector3 c = cross(v1,v2);
q.v = c;
if ( vectors are known to be unit length ) {
    q.w = 1 + dot(v1,v2);
} else {
    q.w = sqrt(v1.length_squared() * v2.length_squared()) + dot(v1,v2);
}
q.normalize();
return q;

--]]

local function dot(u, v)
  return u[1]*v[1]+u[2]*v[2]+u[3]*v[3]
end

local function qvecs3(u, v)
  local q = cross(u, v)
  q[0] = math.sqrt(dot(u,u)*dot(v,v)) + dot(u, v)
  local r = qnorm(q)
  q[0] = q[0] / r
  q[1] = q[1] / r
  q[2] = q[2] / r
  q[3] = q[3] / r
  return q
end

local function qvecs3short(u, v)
  if dot(u,v) < 0 then
    return qvecs3(u, { -v[1], -v[2], -v[3] })
  else
    return qvecs3(u, v)
  end
end

--[[

In the same way the hyperspherical space of 3D rotations can be
parameterized by three angles (Euler angles), but any such parameterization
is degenerate at some points on the hypersphere, leading to the problem of
gimbal lock. We can avoid this by using four Euclidean coordinates w,x,y,z,
with w2 + x2 + y2 + z2 = 1. The point (w,x,y,z) represents a rotation around
the axis directed by the vector (x,y,z) by an angle
\alpha = 2 \cos^{-1} w = 2 \sin^{-1} \sqrt{x^2+y^2+z^2}.

--]]

-- /* 4x4 rotation matrix from one unit quaternion */
-- http://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation#From_a_quaternion_to_an_orthogonal_matrix
local function mquat(q)
  local m = { }
  local a, b, c, d
  a = q[0]; b = q[1]; c = q[2]; d = q[3]
  m[ 0] = a*a+b*b-c*c-d*d
  m[ 1] = 2*b*c-2*a*d
  m[ 2] = 2*b*d+2*a*c
  m[ 3] = 0
  m[ 4] = 2*b*c+2*a*d
  m[ 5] = a*a-b*b+c*c-d*d
  m[ 6] = 2*c*d-2*a*b
  m[ 7] = 0
  m[ 8] = 2*b*d-2*a*c
  m[ 9] = 2*c*d+2*a*b
  m[10] = a*a-b*b-c*c+d*d
  m[11] = 0
  m[12] = 0
  m[13] = 0
  m[14] = 0
  m[15] = 1
  return m
end

-- /* 4x4 rotation matrix from two unit(?) quaternions */
local function mquat2(q, p)
  local m = { }
  local q0, q1, q2, q3, p0, p1, p2, p3;
  q0 = q[0]; q1 = q[1]; q2 = q[2]; q3 = q[3];
  p0 = p[0]; p1 = p[1]; p2 = p[2]; p3 = p[3];
  m[ 0] =   q0*p0 + q1*p1 + q2*p2 + q3*p3;
  m[ 1] =   q1*p0 - q0*p1 - q3*p2 + q2*p3;
  m[ 2] =   q2*p0 + q3*p1 - q0*p2 - q1*p3;
  m[ 3] =   q3*p0 - q2*p1 + q1*p2 - q0*p3;
  m[ 4] = - q1*p0 + q0*p1 - q3*p2 + q2*p3;
  m[ 5] =   q0*p0 + q1*p1 - q2*p2 - q3*p3;
  m[ 6] = - q3*p0 + q2*p1 + q1*p2 - q0*p3;
  m[ 7] =   q2*p0 + q3*p1 + q0*p2 + q1*p3;
  m[ 8] = - q2*p0 + q3*p1 + q0*p2 - q1*p3;
  m[ 9] =   q3*p0 + q2*p1 + q1*p2 + q0*p3;
  m[10] =   q0*p0 - q1*p1 + q2*p2 - q3*p3;
  m[11] = - q1*p0 - q0*p1 + q3*p2 + q2*p3;
  m[12] = - q3*p0 - q2*p1 + q1*p2 + q0*p3;
  m[13] = - q2*p0 + q3*p1 - q0*p2 + q1*p3;
  m[14] =   q1*p0 + q0*p1 + q3*p2 + q2*p3;
  m[15] =   q0*p0 - q1*p1 - q2*p2 + q3*p3;
  return m
end

local function mrotalign(v)
  return mquat(qvecs3(v, { math.sqrt(dot(v,v)), 0, 0 }))
end

local function radius(w)
  return math.sqrt(w[1]*w[1]+w[2]*w[2]+w[3]*w[3]+w[4]*w[4])
end

local function normalize(w)
  local r = radius(w)
  if r ~= 0 then
    return { w[1] / r, w[2] / r, w[3] / r, w[4] / r }
  else
    return { 0, 0, 0, 0 }
  end
end

local function project(w)
  local u = normalize(w)
  local k = 1 / (1.01 - u[4])
  local v = { u[1] * k, u[2] * k, u[3] * k }
  return v
end

local function pol2car(a)
  return { math.cos(a[1]), math.sin(a[1]) * math.cos(a[2]), math.sin(a[1]) * math.sin(a[2]) * math.cos(a[3]), math.sin(a[1]) * math.sin(a[2]) * math.sin(a[3]) }
end

local function car2pol(p)
end

local function vadd(p, q)
  return { p[1]+q[1], p[2]+q[2], p[3]+q[3], p[4]+q[4] }
end

local function vsub(p, q)
  return { p[1]-q[1], p[2]-q[2], p[3]-q[3], p[4]-q[4] }
end

local range = 7
local function note2pol(x, y, z)
  return { math.pi * (x%range) / range, math.pi * (y%range) / range, 2 * math.pi * (z%range) / range }
end

local function delta(p, q, n)
  return { (q[1] - p[1]) / n, (q[2] - p[2]) / n, (q[3] - p[3]) / n, (q[4] - p[4]) / n }
end

function sphere:initialize(sel, atoms)
  if type(atoms[1]) == "number" then
    if atoms[1] >= 1 then
      local r2 = math.sqrt(0.5)
      self.quality = math.floor(atoms[1])
      self.inlets = 2
      self.outlets = 2
      self.cursor   = { { 0, 0, 0 },    { 0, 0, 0 }    }
      self.source   = { { 0, 0, 0, 0 }, { 0, 0, 0, 0 } }
      self.position = { { r2, 0, 0, r2 }, { -r2, 0, 0, r2 } }
      self.target   = { { 0, 0, 0, 0 }, { 0, 0, 0, 0 } }
      self.matrix = {0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1}
      self.matrix[0] = 1
      self:in_2_left({ 1, 0, 0 })
      self:in_2_left({ 1, 0, 0 })
      self:in_2_right({ 5, 0, 0 })
      self:in_2_right({ 5, 0, 0 })
      return true
    end
  end
  return false
end

function sphere:in_2_left(atoms)
  if atoms[1] ~= self.cursor[1][1] or atoms[2] ~= self.cursor[1][2] or atoms[3] ~= self.cursor[1][3] then
    self.source[1] = pol2car(note2pol(self.cursor[1][1], self.cursor[1][2], self.cursor[1][3]))
    self.cursor[1]= { atoms[1], atoms[2], atoms[3] }
  end
  self.target[1] = pol2car(note2pol(atoms[1], atoms[2], atoms[3]))
end

function sphere:in_2_right(atoms)
  if atoms[1] ~= self.cursor[2][1] or atoms[2] ~= self.cursor[2][2] or atoms[3] ~= self.cursor[2][3] then
    self.source[2] = pol2car(note2pol(self.cursor[2][1], self.cursor[2][2], self.cursor[2][3]))
    self.cursor[2]= { atoms[1], atoms[2], atoms[3] }
  end
  self.target[2] = pol2car(note2pol(atoms[1], atoms[2], atoms[3]))
end

local function vtoq(v)
  local q = { }
  q[0] = v[1]
  q[1] = v[2]
  q[2] = v[3]
  q[3] = v[4]
  return q
end

local function mprint(s, v)
  print(s, v[0], v[1], v[2], v[3])
  print(s, v[4], v[5], v[6], v[7])
  print(s, v[8], v[9], v[10], v[11])
  print(s, v[12], v[13], v[14], v[15])
end
local function vprint(s, v)
  print(s, v[1], v[2], v[3], v[4])
end
local function qprint(s, v)
  print(s, v[0], v[1], v[2], v[3])
end

function sphere:in_2_bang()


  -- get starting positions
  local p1 = mmul(self.matrix, self.position[1])
  local q1 = mmul(self.matrix, self.position[2])
  local m1 = normalize(vadd(p1, q1))
--  if m1[4] < 0 then m1 = vsub({0,0,0,0}, m1) end

--  vprint("p1", p1)
--  vprint("q1", q1)
--  vprint("m1", m1)
  
  -- update positions
  local k1 = 0.8
  local k2 = 1 - k1
  for i = 1,2 do
    local s = self.source[i]
    local x = self.position[i]
    local y = self.target[i]
    local z = { x[1] * k1 + k2 * y[1], x[2] * k1 + k2 * y[2], x[3] * k1 + k2 * y[3], x[4] * k1 + k2 * y[4] }
    self.position[i] = normalize(z)
  end

  -- get new positions
  local p2 = mmul(self.matrix, self.position[1])
  local q2 = mmul(self.matrix, self.position[2])
  local m2 = normalize(vadd(p2, q2))
--  if m2[4] < 0 then m2 = vsub({0,0,0,0}, m2) end

--  vprint("p2", p2)
--  vprint("q2", q2)
--  vprint("m2", m2)

  -- find shortest rotation of m2 -> m1
  -- FIXME TODO understand why f1 is needed at all
  local r1 = mquat2(qinv(vtoq(m1)), qinv(vtoq(m2)))
  local f1 = {0,0,0, 0,-1,0,0, 0,0,-1,0, 0,0,0,1}
  f1[0] =1
  r1 = mmulm(f1, r1)
--  mprint("r1", r1)

  -- perform rotate m2 -> m1
  local p3 = mmul(r1, p2)
  local q3 = mmul(r1, q2)
  local m3 = mmul(r1, m2)

--  vprint("p3", p3)
--  vprint("q3", q3)
--  vprint("m3", m3)

  -- find shortest rotation of p3 q3 -> (+/- x4, 0, 0, w4)
  local r2 = mrotalign(p3)

--  mprint("r2", r2)

  -- build resulting matrix
  local r3 = mmulm(r2, r1)
--  mprint("r3", r3)
  local r4 = mmulm(r3, self.matrix)
--  mprint("r4", r4)

  self.matrix = r4

end

function sphere:in_1_list(atoms)
  local x = atoms[1]
  local y = atoms[2]
  local z = atoms[3]
  local w = atoms[4]
  if type(x) == "number" and type(y) == "number" and type(z) == "number" and type(w) == "number" then
    local n = self.quality
    local a = note2pol(x, y, z)
    local b = note2pol(y, z, w)
    local p = pol2car(a)
    local q = pol2car(b)
    local d = delta(p, q, n)
    local s = mmul(self.matrix, p)
    --s       = mmul(self.matrix2, s)
--    s       = mmul(self.matrix3, s)
    local t
    local ss = project(s)
    for i = 1,n do
      t = mmul(self.matrix, { p[1] + d[1] * i, p[2] + d[2] * i, p[3] + d[3] * i, p[4] + d[4] * i })
--      t = mmul(self.matrix2, t)
--      t = mmul(self.matrix3, t)
      local tt = project(t)
      if ss ~= nil and tt ~= nil then
        self:outlet(2, "list", ss)
        self:outlet(1, "list", tt)
      end
      s = t
      ss = tt
    end
  end
end
