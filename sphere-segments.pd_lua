local sphere = pd.Class:new():register("sphere-segments")

local function mmul(l, r)
  local v = { }
  for i = 0,3 do
    local f = 0.0;
    for j = 0,3 do
      f = f + l[i*4+j] * r[j+1];
    end
    v[i+1] = f;
  end
  return v
end

-- /* quaternion norm squared */
local function qnorm2(p)
  return p[0]*p[0]+p[1]*p[1] + p[2]*p[2] + p[3]*p[3];
end

-- /* quaternion norm */
local function qnorm(p)
  return math.sqrt(qnorm2(p));
end

-- /* quaternion inverse: r := p^-1 */
-- /* p^-1 = p* / |p|^2 */
local function qinv(p)
  local pn2 = qnorm2(p);
  local r = { }
  r[0] =  p[0] / pn2;
  r[1] = -p[1] / pn2;
  r[2] = -p[2] / pn2;
  r[3] = -p[3] / pn2;
  return r;
end

-- /* quaternion multiplication: r := q * p */
local function qmul(q, p)
  local q1, q2, q3, q4, p1, p2, p3, p4, r1, r2, r3, r4;
  q1 = q[0]; q2 = q[1]; q3 = q[2]; q4 = q[3];
  p1 = p[0]; p2 = p[1]; p3 = p[2]; p4 = p[3];
  r1 = q1 * p1 - q2 * p2 - q3 * p3 - q4 * p4;
  r2 = q2 * p1 + q1 * p2 - q4 * p3 + q3 * p4;
  r3 = q3 * p1 + q4 * p2 + q1 * p3 - q2 * p4;
  r4 = q4 * p1 - q3 * p2 + q2 * p3 + q1 * p4;
  local r = { }
  r[0] = r1; r[1] = r2; r[2] = r3; r[3] = r4;
  return r
end

-- /* quaternion exponentiation: r := exp(p) */
-- /* exp(s;v) = exp(s) (cos(|v|) ; v sin(|v|) / |v|) */
local function qexp(p)
  local s, v0, v1, v2, vn, se, vns, vnc;
  s   = p[0];
  v0  = p[1];
  v1  = p[2];
  v2  = p[3];
  vn  = math.sqrt(v0*v0 + v1*v1 + v2*v2);
  vnc = math.cos(vn);
  vns = math.sin(vn) / vn;
  se  = math.exp(s);
  local r = { }
  r[0] = se * vnc;
  r[1] = v0 * vns;
  r[2] = v1 * vns;
  r[3] = v2 * vns;
  return r
end

-- /* quaternion logarithm: r := log(p) */
-- /* ln(q@(s;v)) = (ln(|q|) ; v acos(s / |q|) / |v|) */
local function qlog(p)
  local s, v0, v1, v2, vn2, vn, qn, qnl, sac;
  s   = p[0];
  v0  = p[1];
  v1  = p[2];
  v2  = p[3];
  vn2 = v0*v0 + v1*v1 + v2*v2;
  vn  = math.sqrt(vn2);
  qn  = math.sqrt(s*s + vn2);
  qnl = math.log(qn);
  sac = math.acos(s / qn) / vn;
  local r = { }
  r[0] = qnl;
  r[1] = v0 * sac;
  r[2] = v1 * sac;
  r[3] = v2 * sac;
  return r;
end

-- /* quaternion scalar power: r := p^f */
-- /* p^f = exp(ln(p) f) */
local function qpows(p, f)
  local l = qlog(p);
  l[0] = l[0] * f;
  l[1] = l[1] * f;
  l[2] = l[2] * f;
  l[3] = l[3] * f;
  return qexp(l);
end

-- /* quaternion slerp: r := Slerp(p,q,t) */
-- /* Slerp(p,q;t) = (q p^-1)^t p */
local function qslerp(p, q, t)
  return qmul(qpows(qmul(q, qinv(p)), t), p)
end

-- /* 4x4 rotation matrix from two unit(?) quaternions */
local function mquat2(q, p)
  local m = { }
  local q0, q1, q2, q3, p0, p1, p2, p3;
  q0 = q[0]; q1 = q[1]; q2 = q[2]; q3 = q[3];
  p0 = p[0]; p1 = p[1]; p2 = p[2]; p3 = p[3];
  m[ 0] =   q0*p0 + q1*p1 + q2*p2 + q3*p3;
  m[ 1] =   q1*p0 - q0*p1 - q3*p2 + q2*p3;
  m[ 2] =   q2*p0 + q3*p1 - q0*p2 - q1*p3;
  m[ 3] =   q3*p0 - q2*p1 + q1*p2 - q0*p3;
  m[ 4] = - q1*p0 + q0*p1 - q3*p2 + q2*p3;
  m[ 5] =   q0*p0 + q1*p1 - q2*p2 - q3*p3;
  m[ 6] = - q3*p0 + q2*p1 + q1*p2 - q0*p3;
  m[ 7] =   q2*p0 + q3*p1 + q0*p2 + q1*p3;
  m[ 8] = - q2*p0 + q3*p1 + q0*p2 - q1*p3;
  m[ 9] =   q3*p0 + q2*p1 + q1*p2 + q0*p3;
  m[10] =   q0*p0 - q1*p1 + q2*p2 - q3*p3;
  m[11] = - q1*p0 - q0*p1 + q3*p2 + q2*p3;
  m[12] = - q3*p0 - q2*p1 + q1*p2 + q0*p3;
  m[13] = - q2*p0 + q3*p1 - q0*p2 + q1*p3;
  m[14] =   q1*p0 + q0*p1 + q3*p2 + q2*p3;
  m[15] =   q0*p0 - q1*p1 - q2*p2 + q3*p3;
  return m
end

local function radius(w)
  return math.sqrt(w[1]*w[1]+w[2]*w[2]+w[3]*w[3]+w[4]*w[4])
end

local function normalize(w)
  local r = radius(w)
  return { w[1] / r, w[2] / r, w[3] / r, w[4] / r }
end

local function project(w)
  local u = normalize(w)
  local k = 2 / (2 - u[4])
  local v = { u[1] * k, u[2] * k, u[3] * k }
  return v
end

local function pol2car(a)
  return { math.cos(a[1]), math.sin(a[1]) * math.cos(a[2]), math.sin(a[1]) * math.sin(a[2]) * math.cos(a[3]), math.sin(a[1]) * math.sin(a[2]) * math.sin(a[3]) }
end

local function car2pol(p)
end

local function vadd(p, q)
  return { p[1]+q[1], p[2]+q[2], p[3]+q[3], p[4]+q[4] }
end

local function vsub(p, q)
  return { p[1]-q[1], p[2]-q[2], p[3]-q[3], p[4]-q[4] }
end

local range = 7
local function note2pol(x, y, z)
  return { math.pi * (x%range) / range, math.pi * (y%range) / range, 2 * math.pi * (z%range) / range }
end

local function delta(p, q, n)
  return { (q[1] - p[1]) / n, (q[2] - p[2]) / n, (q[3] - p[3]) / n, (q[4] - p[4]) / n }
end

function sphere:initialize(sel, atoms)
  if type(atoms[1]) == "number" then
    if atoms[1] >= 1 then
      self.quality = math.floor(atoms[1])
      self.inlets = 2
      self.outlets = 2
      self.cursor = { 0, 0, 0}
      self.source = pol2car(note2pol(0,0,0))
      self.angles = { 0, 0, 0 }
      self.position = { 0, 0, 0, 0 }
      self:in_2_list({ 0, 0, 0 })
      self:in_2_list({ 0, 0, 0 })
      self.matrix1 = {0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1}
      self.matrix1[0] = 1
      self.matrix2 = {0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1}
      self.matrix2[0] = 1
      self.matrix3 = {0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1}
      self.matrix3[0] = 1
      return true
    end
  end
  return false
end

function sphere:in_2_list(atoms)
--  self.source = self.target
--  self.position = self.source
  if atoms[1] ~= self.cursor[1] or atoms[2] ~= self.cursor[2] or atoms[3] ~= self.cursor[3] then
    self.source = pol2car(note2pol(self.cursor[1], self.cursor[2], self.cursor[3]))
    self.cursor = { atoms[1], atoms[2], atoms[3] }
  end
  self.target = pol2car(note2pol(atoms[1], atoms[2], atoms[3]))
end

function sphere:in_2_bang()
  local s = self.source
  local x = self.position
  local y = self.target
  local k1 = 0.7
  local k2 = 1 - k1
  local z = { x[1] * k1 + k2 * y[1], x[2] * k1 + k2 * y[2], x[3] * k1 + k2 * y[3], x[4] * k1 + k2 * y[4] }
  z = normalize(z)
  self.position = z
  local p = { }
  p[0] = z[1]
  p[1] = z[2]
  p[2] = z[3]
  p[3] = z[4]
  local o = { }
  o[0] = 0
  o[1] = 0
  o[2] = 0
  o[3] = -1
  self.matrix1 = mquat2(o, qinv(p))
  local l = mmul(self.matrix1, { 0, 0, 0, 1})
  local q = { }
  q[0] = l[1]
  q[1] = l[2]
  q[2] = l[3]
  q[3] = l[4]
  self.matrix2 = mquat2(qinv(q), o)
  self.matrix3 = mquat2(qinv(p), o)
end

function sphere:in_1_list(atoms)
  local x = atoms[1]
  local y = atoms[2]
  local z = atoms[3]
  local w = atoms[4]
  local m = self.angles
  if type(x) == "number" and type(y) == "number" and type(z) == "number" and type(w) == "number" then
    local n = self.quality
    local a = note2pol(x, y, z)
    local b = note2pol(y, z, w)
    local p = pol2car(a)
    local q = pol2car(b)
    local d = delta(p, q, n)
    local s = mmul(self.matrix1, p)
--    s       = mmul(self.matrix2, s)
--    s       = mmul(self.matrix3, s)
    local t
    local ss = project(s)
    for i = 1,n do
      t = mmul(self.matrix1, { p[1] + d[1] * i, p[2] + d[2] * i, p[3] + d[3] * i, p[4] + d[4] * i })
--      t = mmul(self.matrix2, t)
--      t = mmul(self.matrix3, t)
      local tt = project(t)
      if ss ~= nil and tt ~= nil then
        self:outlet(2, "list", ss)
        self:outlet(1, "list", tt)
      end
      s = t
      ss = tt
    end
  end
end
