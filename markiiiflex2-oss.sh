#!/bin/bash
pd -stderr -verbose -r 48000 -oss -audiobuf 500 \
-path /usr/lib/pd/extra/lua/ -lib lua \
-path /usr/lib/pd/extra/Gem/ -lib Gem \
-path /usr/lib/pd/extra/zexy/ -lib zexy \
-path /usr/lib/pd/extra/iemmatrix/ -lib iemmatrix \
-open markiiiflex2.pd
