#!/bin/bash
pd -stderr -verbose -alsa -audiodev 6 -channels 2 -audiobuf 500 \
-path /usr/lib/pd/extra/lua/ -lib lua \
-path /usr/lib/pd/extra/Gem/ -lib Gem \
-path /usr/lib/pd/extra/zexy/ -lib zexy \
-open markiiiflex2.pd
