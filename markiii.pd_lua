local markiii = pd.Class:new():register("markiii")
local shared = { }

math.randomseed(4321234)

function markiii:initialize(sel, atoms)
  if type(atoms[1]) == "string" then
    if shared[atoms[1]] == nil then
      local chain = { }
      chain[0] = { }
      chain[0][0] = { }
      chain[0][0][0] = { total = 0 }
      local visited = { }
      visited[0] = { }
      visited[0][0] = { }
      visited[0][0][0] = { }
      shared[atoms[1]] = { chain = chain, visited = visited, count = 1 }
    else
      shared[atoms[1]].count = shared[atoms[1]].count + 1
    end
    self.name = atoms[1]
    self.ri = 0
    self.rj = 0
    self.rk = 0
    self.wi = 0
    self.wj = 0
    self.wk = 0
    self.inlets = 3
    self.outlets = 3
    return true
  else
    return false
  end
end

function markiii:in_1_set(atoms)
  self.ri = atoms[1]
  self.rj = atoms[2]
  self.rk = atoms[3]
end

function markiii:in_2_set(atoms)
  self.wi = atoms[1]
  self.wj = atoms[2]
  self.wk = atoms[3]
end

function markiii:in_3_bang()
  self:outlet(3, "read",  { self.ri, self.rj, self.rk })
  self:outlet(3, "write", { self.wi, self.wj, self.wk })
  for i,ti in pairs(shared[self.name].chain) do
    for j,tj in pairs(ti) do
      for k,tk in pairs(tj) do
        for l,p in pairs(tk) do if l ~= "total" then
          local v = 1
          if nil ~= shared[self.name].visited[i] and nil ~= shared[self.name].visited[i][j] and nil ~= shared[self.name].visited[i][j][k] and nil ~= shared[self.name].visited[i][j][k][l] then
            v = shared[self.name].visited[i][j][k][l]
          end
          self:outlet(3, "point", { i, j, k, l, p / tk.total, v })
  end end end end end
end

function markiii:in_1_bang()
  local t = shared[self.name].chain[self.ri][self.rj][self.rk]
  if t.total == 0 then
    self:outlet(2, "bang", { })
    return
  end
  local p = math.random() * t.total
  local q = 0
  local f = 0
  for k,v in pairs(t) do if k ~= "total" then
    q = q + v
    if p <= q then
      f = k
      break
    end
  end end
  if shared[self.name].visited[self.ri] == nil then shared[self.name].visited[self.ri] = { } end
  if shared[self.name].visited[self.ri][self.rj] == nil then shared[self.name].visited[self.ri][self.rj] = { } end
  if shared[self.name].visited[self.ri][self.rj][self.rk] == nil then shared[self.name].visited[self.ri][self.rj][self.rk] = { } end
  if shared[self.name].visited[self.ri][self.rj][self.rk][f] == nil then shared[self.name].visited[self.ri][self.rj][self.rk][f] = 0 end
  shared[self.name].visited[self.ri][self.rj][self.rk][f] = shared[self.name].visited[self.ri][self.rj][self.rk][f] + 1
  self.ri = self.rj
  self.rj = self.rk
  self.rk = f
  if shared[self.name].chain[self.ri] == nil then shared[self.name].chain[self.ri] = { } end
  if shared[self.name].chain[self.ri][self.rj] == nil then shared[self.name].chain[self.ri][self.rj] = { } end
  if shared[self.name].chain[self.ri][self.rj][self.rk] == nil then shared[self.name].chain[self.ri][self.rj][self.rk] = { total = 0 } end
  self:outlet(1, "float", { f })
end

function markiii:in_2_float(f)
  local n = shared[self.name].chain[self.wi][self.wj][self.wk][f]
  local t = shared[self.name].chain[self.wi][self.wj][self.wk].total
  if n == nil then n = 0 end
  shared[self.name].chain[self.wi][self.wj][self.wk][f] = n + 1
  shared[self.name].chain[self.wi][self.wj][self.wk].total = t + 1
  self.wi = self.wj
  self.wj = self.wk
  self.wk = f
  if shared[self.name].chain[self.wi] == nil then shared[self.name].chain[self.wi] = { } end
  if shared[self.name].chain[self.wi][self.wj] == nil then shared[self.name].chain[self.wi][self.wj] = { } end
  if shared[self.name].chain[self.wi][self.wj][self.wk] == nil then shared[self.name].chain[self.wi][self.wj][self.wk] = { total = 0 } end
end

function markiii:in_2_symbol(s)
  if s == "total" then self:error("'total' not allowed") else self:in_2_float(s) end
end

function markiii:finalize()
  shared[self.name].count = shared[self.name].count - 1
  if shared[self.name].count == 0 then
    shared[self.name] = nil
  end
end

