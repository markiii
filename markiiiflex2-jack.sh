#!/bin/bash
pd -stderr -verbose -jack -channels 2 \
-path /usr/lib/pd/extra/lua/ -lib lua \
-path /usr/lib/pd/extra/Gem/ -lib Gem \
-path /usr/lib/pd/extra/zexy/ -lib zexy \
-open markiiiflex2.pd
